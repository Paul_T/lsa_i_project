package cs.ucc.dc1.datacentre.manager.migration;

import java.util.List;

import cs.ucc.dc1.datacentre.bean.CPU;
import cs.ucc.dc1.datacentre.bean.Job;
import cs.ucc.dc1.utils.Configuration;

/** Moves jobs from a cpu to another one  */
public class MigratorManager {
	
	public Boolean migrateJobToAnotherCPU(List<Job> jobs, List<CPU> cpus,CPU cpuSelected) {
		
		System.out.println("\t\t" + this.getClass().getName());
		Boolean hasNotMigratedJobs = true;
		for(Job jobSelected : jobs) {
			if(jobSelected.getCpuLoad() <= (Configuration.CPU_MAX_LOAD - cpuSelected.getCpuLoad())) {
				if(migrate(jobSelected,cpus,cpuSelected)) {
					hasNotMigratedJobs = false;
				}
			}
			
			if(cpuSelected.getCpuLoad() > Configuration.CPU_MAX_ALLOWED_LOAD)
				return hasNotMigratedJobs;
		}
		return hasNotMigratedJobs;
	}
	
	private Boolean migrate(Job jobSelected, List<CPU> cpus, CPU cpuSelected) {
		
		System.out.println("*** MIGRATION *** ");
		
		for(CPU cpu : cpus) {
			if(cpu.getJobs().contains(jobSelected)) {
				cpuSelected.migrateJobToNewCPU(jobSelected);
				cpu.migrateJob(jobSelected);
				return true;
			}
		}
		return false;
	}
}
