package cs.ucc.dc1.datacentre.manager;

import java.util.ArrayList;
import java.util.List;

import cs.ucc.dc1.datacentre.bean.Job;
import cs.ucc.dc1.utils.TestSettings;

/**
 * Schedules jobs in a CPU
 *
 */
public class Scheduler {
	
	/**sort jobs according to their priority and remaining execution time
	 * 
	 * @param jobs needing to be scheduled
	 */
	public List<Job> scheduleJobs(List<Job> jobs) {
		System.out.println("\t\t" + this.getClass().getName());
	
		List<Job> tempJobs = new ArrayList<Job>();
		tempJobs.addAll(jobs);
		List<Job> sortedJobs = new ArrayList<Job>();
		
		int size = tempJobs.size();
		
		for(int i = 0 ; i < size; i++) {
			Job job = this.getMaxPriorityAndMinExecutionTime(tempJobs);
			sortedJobs.add(job);
			tempJobs.remove(job);
		}
		
		return sortedJobs;
	}
	
	private Job getMaxPriorityAndMinExecutionTime(List<Job> jobs) {
		
		int maxPriority = 0;
		float minExecutionTime = TestSettings.MAX_DURATION_JOB;
		Job job = null;
		
		for(int i = 0 ; i < jobs.size(); i++) {
			if(jobs.get(i).getPriority() > maxPriority) {
				job = jobs.get(i);
				maxPriority = job.getPriority();
				minExecutionTime = job.getRemainingExecutionTime();
			}
			else if (jobs.get(i).getPriority() == maxPriority && jobs.get(i).getRemainingExecutionTime() < minExecutionTime) {
				job = jobs.get(i);
				maxPriority = job.getPriority();
				minExecutionTime = job.getRemainingExecutionTime();
			}
		}
		
		return job;
	}

}
