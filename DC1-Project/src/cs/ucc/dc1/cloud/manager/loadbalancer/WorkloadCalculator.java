package cs.ucc.dc1.cloud.manager.loadbalancer;

import java.util.ArrayList;
import java.util.List;

import cs.ucc.dc1.cloud.bean.DatacentreBean;
import cs.ucc.dc1.datacentre.bean.Job;
import cs.ucc.dc1.utils.Pair;
import cs.ucc.dc1.utils.TestSettings;

public class WorkloadCalculator {

	public List<Pair<String, Job>> findAppropriateDCs(List<Job> jobList, List<DatacentreBean> dcList) {
		System.out.println("\t\t" + this.getClass().getName());
		List<Pair<String, Job>> appropriateDcJobAssociation = new ArrayList<>();
		
		for (Job job : jobList) {
			
			int minWorkload = 0;
			int indexDcSelected = 0;
			
			// /!\ SKIP LOAD BALANCING OPTION IS ONLY FOR THE DEMONSTRATION
			if (!TestSettings.SKIP_LOAD_BALANCE) {
			
				for (int i = 0; i < dcList.size(); i++) {
					int newWorkload = job.getCpuLoad() + dcList.get(i).getWorkload();
					
					if ((i == 0) || (minWorkload > newWorkload)) {
						minWorkload = newWorkload;
						indexDcSelected = i;
					}
				}
			} else {
				// /!\ SKIP LOAD BALANCING OPTION IS ONLY FOR THE DEMONSTRATION
				minWorkload = job.getCpuLoad() + dcList.get(0).getWorkload();
			}
		
			job.setDatacentreID(dcList.get(indexDcSelected).getId());
			
			Pair<String, Job> association = new Pair<>();
			association.setFirst(dcList.get(indexDcSelected).getId());
			association.setSecond(job);
			appropriateDcJobAssociation.add(association);
			dcList.get(indexDcSelected).setWorkload(minWorkload);
			
			// We remove the job from the list
			jobList.remove(job);
		}
		
		return appropriateDcJobAssociation;
	}
	
}
