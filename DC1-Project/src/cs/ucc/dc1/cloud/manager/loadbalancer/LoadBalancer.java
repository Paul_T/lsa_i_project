package cs.ucc.dc1.cloud.manager.loadbalancer;

import java.util.List;

import cs.ucc.dc1.cloud.Cloud;
import cs.ucc.dc1.cloud.bean.DatacentreBean;
import cs.ucc.dc1.datacentre.bean.Job;
import cs.ucc.dc1.utils.Pair;

public class LoadBalancer {

	private WorkloadCalculator workloadCalculator = new WorkloadCalculator();
	private JobAssignor jobAssignor = new JobAssignor();
	
	public void associateJobs(Cloud cloud, List<Job> jobList, List<DatacentreBean> dcList) {
		System.out.println("\t" + this.getClass().getName());
		List<Pair<String, Job>> associations = workloadCalculator.findAppropriateDCs(jobList, dcList);
		jobAssignor.associateJobs(cloud, associations);
	}
}
