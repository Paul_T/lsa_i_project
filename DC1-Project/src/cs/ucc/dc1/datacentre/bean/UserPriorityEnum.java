package cs.ucc.dc1.datacentre.bean;

/** Value representing the priority of a job */
public enum UserPriorityEnum {
	USER1(1),
	USER2(2),
	USER3(3);
	
	private int priority;
	
	UserPriorityEnum(int load) {
		this.priority = load;
	} 
	
	public int getPriority() {
		return priority;
	}
}
