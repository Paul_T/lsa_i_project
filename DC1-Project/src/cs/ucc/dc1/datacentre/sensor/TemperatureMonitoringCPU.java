package cs.ucc.dc1.datacentre.sensor;

import cs.ucc.dc1.datacentre.Datacentre;
import cs.ucc.dc1.datacentre.Datacentre.CoolingSystemEnum;
import cs.ucc.dc1.datacentre.bean.CPU;
import cs.ucc.dc1.utils.Configuration;

/** Monitors the temperature of CPU */
public class TemperatureMonitoringCPU {

	public void monitorTemperature(Datacentre dc) {
		
		boolean hasOneCpuHot = false;
		
		for (CPU cpu : dc.getDatacentreDatabase().getCpus()) {
			if (cpu.getTemperature() >= Configuration.CPU_TRESHOLD_COOLING_SYSTEM) {
				dc.setCoolingSystem(CoolingSystemEnum.ON);
				return;
			} else if (cpu.getTemperature() > Configuration.CPU_MIN_TEMPERATURE) {
				hasOneCpuHot = true;
			}
		}
		
		// Cooling system ON and CPU not totally cooled, we keep cooling
		if (dc.getCoolingSystem().equals(CoolingSystemEnum.ON) && hasOneCpuHot) {
			return;
		}
		
		dc.setCoolingSystem(CoolingSystemEnum.OFF);
	}
}
