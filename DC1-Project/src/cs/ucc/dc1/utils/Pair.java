package cs.ucc.dc1.utils;

public class Pair<T, K> {

	private T first;
	private K second;
	
	public T getFirst() {
		return first;
	}
	public void setFirst(T first) {
		this.first = first;
	}
	public K getSecond() {
		return second;
	}
	public void setSecond(K second) {
		this.second = second;
	}
	
	
}
