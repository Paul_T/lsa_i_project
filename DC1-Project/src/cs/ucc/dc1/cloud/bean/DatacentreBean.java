package cs.ucc.dc1.cloud.bean;

import java.math.BigDecimal;

import cs.ucc.dc1.utils.Configuration;
import javafx.application.Platform;
import javafx.scene.control.Label;

/** Represents the data known by the cloud about the datacenters */
public class DatacentreBean {
	
	/** GENERAL **/
	private String id;
	private DatacenterStatusEnum status;
	private int workload;
	
	private Label statusLabel;
	private Label workloadLabel;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public DatacenterStatusEnum getStatus() {
		return status;
	}
	
	public void setStatus(DatacenterStatusEnum status) {
		this.status = status;
		if (statusLabel != null) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					statusLabel.setText(status.name());	
				}
			});
		}
	}
	
	public int getWorkload() {
		return workload;
	}
	
	public void updateDcStatus() {
		if (getWorkload() >= Configuration.DC_THRESHOLD_LIMITED_STATUS) {
			setStatus(DatacenterStatusEnum.LIMITING);
		} else {
			setStatus(DatacenterStatusEnum.UNRESTRICTED);
		}
	}
	
	public void setWorkload(int workload) {
		this.workload = workload;
		
		updateDcStatus();
		
		if (workloadLabel != null) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					workloadLabel.setText(getWorkloadAsPercentage(workload) + "%");
				}
			});
		}
	}

	public Label getStatusLabel() {
		return statusLabel;
	}

	public void setStatusLabel(Label statusLabel) {
		this.statusLabel = statusLabel;
	}

	public Label getWorkloadLabel() {
		return workloadLabel;
	}

	public void setWorkloadLabel(Label workloadLabel) {
		this.workloadLabel = workloadLabel;
	}
	
	public void decreaseWorkload(int i) {
		int workload = this.workload - i;
		this.setWorkload(workload);
	}
	
	private String getWorkloadAsPercentage(int workload) {
		float workloadAsPercentage = ((float) workload) / ((float)Configuration.DC_MAX_ALLOWED_LOAD) * 100;
		return Float.toString(new BigDecimal(workloadAsPercentage >= 100 ? 100 : workloadAsPercentage)
				.setScale(2, BigDecimal.ROUND_HALF_UP)
				.floatValue());
	
	}
}
