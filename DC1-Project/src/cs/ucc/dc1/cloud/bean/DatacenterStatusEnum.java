package cs.ucc.dc1.cloud.bean;

public enum DatacenterStatusEnum {
	UNRESTRICTED,
	LIMITING
}
