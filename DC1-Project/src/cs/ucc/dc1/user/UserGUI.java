package cs.ucc.dc1.user;

import java.io.IOException;
import java.util.UUID;

import org.codehaus.jackson.map.ObjectMapper;

import cs.ucc.dc1.cloud.Cloud;
import cs.ucc.dc1.datacentre.bean.Job;
import cs.ucc.dc1.datacentre.bean.JobLoadEnum;
import cs.ucc.dc1.datacentre.bean.UserPriorityEnum;
import javafx.scene.Group;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public class UserGUI {
	
	/** GUI JAVA FX **/
	private Stage userStage;
	private Group root;
	private TextArea console;
	
	/** GENERAL **/
	private String userID;
	
	/** CONNECTION WITH CLOUD **/
	private Cloud cloud;
	
	public UserGUI(Stage userStage, Group root, TextArea console, String id) {
		this.userStage = userStage;
		this.root = root;
		this.console = console;
		this.userID = id;
	}
	
	public void setCloud(Cloud cloud) {
		this.cloud = cloud;
	}
	
	public void createJob(int environment, String jobDuration, String executableName, String username) 
			throws ExecutableNameException, UsernameException, JobDurationException {
		
		float duration = Float.parseFloat(jobDuration);		
		String environmentName = JobLoadEnum.values()[environment-1].toString();
		
		if (duration == 0 || duration > 672)
			throw new JobDurationException();
		
		Job job = new Job();
		job.setStartingExecutionTime(duration);
		job.setRemainingExecutionTime(duration);
		
		if (executableName == null || executableName.length() == 0 || !executableName.contains("."))
			throw new ExecutableNameException();
		
		if (username == null || username.length() == 0)
			throw new UsernameException();
		
		job.setName(executableName);
		job.setUserId(userID);
		job.setEnvironment(environmentName);
		job.setCpuLoad(JobLoadEnum.valueOf(environmentName).getLoad());
		job.setPriority(UserPriorityEnum.valueOf(username).getPriority());
		//job.setJobId(UUID.randomUUID().toString().replaceAll("-", "").substring(0,7));
		sendJob(job);
	}

	private void sendJob(Job job) {
	
		ObjectMapper mapper = new ObjectMapper();
		String jobAsString;
		try {
			writeIntoConsole("Your job has been sent! ID: " + job.getJobId());
			jobAsString = mapper.writeValueAsString(job);
			cloud.simulateJobReception(jobAsString);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeIntoConsole(String text) {
		console.appendText(text);
		console.appendText("\n");
	}
	
	public class ExecutableNameException extends Exception {
		private static final long serialVersionUID = 1L;
		
	}
	
	public class UsernameException extends Exception {
		private static final long serialVersionUID = 1L;
		
	}
	
	public class JobDurationException extends Exception {
		private static final long serialVersionUID = 1L;
		
	}
}
