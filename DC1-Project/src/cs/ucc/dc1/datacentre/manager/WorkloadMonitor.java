package cs.ucc.dc1.datacentre.manager;

import cs.ucc.dc1.datacentre.Datacentre;
import cs.ucc.dc1.datacentre.bean.CPU;
import cs.ucc.dc1.datacentre.bean.Job;
import cs.ucc.dc1.utils.Configuration;

/** Responsible of the global workload and determines if a migration is necessary or not */
public class WorkloadMonitor {

	/**Represents the global load of the DC */
	private int globalWorkload = 0;
	private int globalRunningWorkload = 0;
	private Datacentre datacentre;
	
	public WorkloadMonitor(Datacentre datacentre) {
		this.datacentre = datacentre;
	}

	/**
	 * @param job updates the average load of the cpu
	 * @return migration or not
	 */
	public void calculateNewGlobalWorkload(Job job) {
		globalWorkload += job.getCpuLoad();
		calculateGlobalRunningWorkload();
	}
	
	private void calculateGlobalRunningWorkload() {
		int i = 0;
		for(CPU cpu : datacentre.getDatacentreDatabase().getCpus()) {
			for(Job job : cpu.getJobs())
				i += job.getCpuLoad();
		}
		
		globalRunningWorkload = i;
	}
	
	/** @return migration or not */
	public Boolean isMigrationWorthIt(Job job) {
		System.out.println("\t" + this.getClass().getName() + " : Job (" + job.getJobId() + ")");
		
		for(CPU cpu : datacentre.getDatacentreDatabase().getCpus()) {
			// if a cpu is in a critical situation
			if(cpu.getTemperature() > Configuration.CPU_MAX_ALLOWED_TEMPERATURE
					|| cpu.getCpuHistoricalUse() > Configuration.CPU_MAX_ALLOWED_AVERAGE_LOAD ) {
				
				cpu.writeIntoConsole("/!\\ critical state /!\\ ");
				return true;
			}
		}
		
		if(!hasEnoughPlaceInCPU(job) && globalRunningWorkload < Configuration.DC_MAX_ALLOWED_LOAD)
			return true;
			
		return false;
	}
	
	private Boolean hasEnoughPlaceInCPU(Job job) {
		for(CPU cpu : datacentre.getDatacentreDatabase().getCpus()) {
			if((cpu.getCpuLoad() + job.getCpuLoad()) <= Configuration.CPU_MAX_ALLOWED_LOAD)
				return true;
		}
		return false;
	}

	public void removeJobLoadFromDCLoad(Job job) {
		globalWorkload -= job.getCpuLoad();
	}

	public int getGlobalWorkload() {
		return globalWorkload;
	}

	public int getGlobalRunningWorkload() {
		return globalRunningWorkload;
	}
	
}
