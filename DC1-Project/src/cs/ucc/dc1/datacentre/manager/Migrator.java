package cs.ucc.dc1.datacentre.manager;

import java.util.ArrayList;
import java.util.List;

import cs.ucc.dc1.datacentre.bean.CPU;
import cs.ucc.dc1.datacentre.bean.Job;
import cs.ucc.dc1.datacentre.manager.migration.CPUSorter;
import cs.ucc.dc1.datacentre.manager.migration.CPUStatusMonitor;
import cs.ucc.dc1.datacentre.manager.migration.JobSorter;
import cs.ucc.dc1.datacentre.manager.migration.MigratorManager;
import cs.ucc.dc1.utils.Configuration;

/**
 *	Migrate jobs from CPU to others to optimize the available place
 */
public class Migrator {
	
	public Boolean migrate(List<CPU> cpus) {
		System.out.println("\t" + this.getClass().getName());
		
		List<CPU> localCPUs = new ArrayList<CPU>(cpus);
		
		CPUSorter cpuSorter = new CPUSorter();
		CPUStatusMonitor cpuStatusMonitor = new CPUStatusMonitor();
		JobSorter jobSorter = new JobSorter();
		MigratorManager migratorManager = new MigratorManager();
		
		int noOfCpus = cpus.size();
		
		
		// the algorithm of migration may meet issues because of the low number of cpus 
		
		// it is designed for big datacentres, and because of the small amount of CPUs we may end up in paradoxal situations that 
		// wouldn't be met in real datacentres
		Boolean hasChanged = false;
		for(int i = 0 ; i < noOfCpus ; i++) {
		
			List<CPU> sortedCPUs = cpuSorter.sortCPUByCPUUsage(localCPUs);
			cpuStatusMonitor.updateStatusOfCPUs(localCPUs);
			
			CPU cpuSelected = sortedCPUs.get(0);
			
			if(Configuration.CPU_MAX_LOAD - cpuSelected.getCpuLoad() >= Configuration.CPU_LIMIT_LOWEST_VALUE && !cpuStatusMonitor.hasAnyCriticalCPU(localCPUs))
				break;
			
			List<Job> sortedJobs = jobSorter.sortJobsByOwnerLoad(localCPUs,cpuSelected.getIdCPU());
			
			cpuSelected.writeIntoConsole("\t+++   Migration   +++");
			if(!migratorManager.migrateJobToAnotherCPU(sortedJobs, localCPUs, cpuSelected))
				hasChanged = true;
			cpuSelected.writeIntoConsole("\t+++ End Migration +++");
		}
		return hasChanged;
	}

}
