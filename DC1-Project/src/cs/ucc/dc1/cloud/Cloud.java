package cs.ucc.dc1.cloud;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import cs.ucc.dc1.cloud.bean.DatacenterStatusEnum;
import cs.ucc.dc1.cloud.bean.DatacentreBean;
import cs.ucc.dc1.cloud.dao.CloudDatabase;
import cs.ucc.dc1.cloud.manager.loadbalancer.LoadBalancer;
import cs.ucc.dc1.cloud.thread.JobProgListenerThread;
import cs.ucc.dc1.cloud.thread.LoadBalancerThread;
import cs.ucc.dc1.cloud.thread.UserListenerThread;
import cs.ucc.dc1.datacentre.Datacentre;
import cs.ucc.dc1.datacentre.bean.Job;
import cs.ucc.dc1.user.UserGUI;
import cs.ucc.dc1.utils.Configuration;
import cs.ucc.dc1.utils.TestSettings;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public class Cloud {
	
	/** GUI JAVA FX **/
	private Stage cloudStage;
	private Group root;
	private TextArea console;
	
	/** GENERAL **/
	private List<DatacentreBean> dcList = new ArrayList<>();
	private List<UserGUI> userList = new ArrayList<>();
	
	private CloudDatabase cloudDb = new CloudDatabase();
	
	/** COMMUNICATION **/
	private JobProgListenerThread jobProgListenerThread;
	private UserListenerThread userListenerThread;
	
	/** LOAD BALANCING **/
	private LoadBalancerThread loadBalancingThread;
	
	private LoadBalancer loadBalancer = new LoadBalancer();
	
	/** JOB LIST **/
	private List<Job> waitingJobList = new CopyOnWriteArrayList<Job>();
	
	public Cloud(Stage cloudStage, Group root, TextArea console) {
		this.cloudStage = cloudStage;
		this.root = root;
		this.console = console;
		
		userListenerThread = new UserListenerThread(this);
		userListenerThread.setRunning(true);
		userListenerThread.start();
		
		loadBalancingThread = new LoadBalancerThread(this);
		loadBalancingThread.setRunning(true);
		loadBalancingThread.start();
		
		jobProgListenerThread = new JobProgListenerThread(this);
		jobProgListenerThread.setRunning(true);
		jobProgListenerThread.start();
	}
	
	public Cloud() {
		
	}
	
	public void addDatacentreBean(DatacentreBean dc) {
		dcList.add(dc);
	}
	
	public DatacentreBean getDatacentreBean(int index) {
		return dcList.get(index);
	}
	
	public void addConnectedDatacentre(Datacentre dc) {
		cloudDb.getConnectedDcList().add(dc);
	}
	
	public void addUserGUI(UserGUI user) {
		userList.add(user);
	}
	
	public UserGUI getUserGUI(int position) {
		if (position >= userList.size()) {
			return null;
		}
		
		return userList.get(position);
	}
	
	public void simulateJobReception(String jobAsString) {
		userListenerThread.pushJob(jobAsString);
	}
	
	public void simulateJobProgReception(String jobAsString) {
		jobProgListenerThread.pushJob(jobAsString);
	}
	
	public void appendToJobList(Job job) {
		System.out.println(this.getClass().getName() + " : Job (" + job.getJobId() + ")");
		
		waitingJobList.add(job);
		if (Configuration.CLOUD_TRIGGER_LOAD_BALANCING <= waitingJobList.size()) {
			// Trigger the load balancing
			loadBalancingThread.release();
		}
	}

	public void simulateJobRouting(String jobAsString, String dcId) {
		
		for (Datacentre dc : cloudDb.getConnectedDcList()) {
			if (dc.getId().equals(dcId)) {
				dc.simulateJobReception(jobAsString);
				break;
			}
		}
	}
	
	public void startLoadBalancing() {
		// /!\ SKIP LOAD BALANCING OPTION IS ONLY FOR THE DEMONSTRATION
		if (TestSettings.SKIP_LOAD_BALANCE) {
			try {
				writeIntoConsole("Load balancing in SKIPPED");
			} catch(IllegalStateException e) {
				e.printStackTrace();
			}
			
			loadBalancer.associateJobs(this, waitingJobList, dcList);
			return;
		} else {
			try {
				writeIntoConsole("Load balancing in progress...");
			} catch(IllegalStateException e) {
				e.printStackTrace();
			}
		}
		
		List<DatacentreBean> dcToUseList = new ArrayList<>();
		
		for (DatacentreBean dc : dcList) {
			if (dc.getStatus().equals(DatacenterStatusEnum.UNRESTRICTED)) {
				dcToUseList.add(dc);
			}
		}
		
		if (dcToUseList.isEmpty()) {
			// All DC are LIMITING
			loadBalancer.associateJobs(this, waitingJobList, dcList);
		} else {
			// At least 1 DC is UNRESTRICTED, it/they has/have the priority to handle the jobs
			loadBalancer.associateJobs(this, waitingJobList, dcToUseList);
		}		
	}
	
	public void writeIntoConsole(String text) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				console.appendText(text);
				console.appendText("\n");
			}
		});
	}
	
	public DatacentreBean getDatacentreById(String id) {
		for(DatacentreBean dc : dcList) {
			if(id.equals(dc.getId()))
				return dc;
		}
		return null;
	}

	public List<DatacentreBean> getDcList() {
		return dcList;
	}

	public void setDcList(List<DatacentreBean> dcList) {
		this.dcList = dcList;
	}

	public LoadBalancer getLoadBalancer() {
		return loadBalancer;
	}

	public void setLoadBalancer(LoadBalancer loadBalancer) {
		this.loadBalancer = loadBalancer;
	}

	public List<Job> getWaitingJobList() {
		return waitingJobList;
	}

	public void setWaitingJobList(List<Job> waitingJobList) {
		this.waitingJobList = waitingJobList;
	}
	
	
}
