package cs.ucc.dc1.cloud.thread;

import java.util.concurrent.Semaphore;

import cs.ucc.dc1.cloud.Cloud;

public class LoadBalancerThread extends Thread {

	private Semaphore semaphore;
	private boolean running;
	private Cloud cloud;
	
	public LoadBalancerThread(Cloud cloud) {
		this.cloud = cloud;
		this.semaphore = new Semaphore(0, true);
	}
	
	@Override
	public void run() {
		
		while (running) {
			try {
				semaphore.acquire();
				cloud.startLoadBalancing();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	public void setRunning(boolean running) {
		this.running = running;
	}
	
	public void release() {
		semaphore.release();
	}
	
}
