package cs.ucc.dc1.datacentre.bean;

/** Value representing the necessary CPU load to execute a job
 */
public enum JobLoadEnum {
	Tomcat(10),
	GlassFish(20),
	JBoss(30),
	WebSphere(40),
	NodeJS(50);
	/*JOB6(60),
	JOB7(70),
	JOB8(80),
	JOB9(90);*/
	 
	private int load;
	
	JobLoadEnum(int load) {
		this.load = load;
	}
	
	public int getLoad() {
		return load;
	}
}
