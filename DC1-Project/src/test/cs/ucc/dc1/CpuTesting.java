package test.cs.ucc.dc1;

import static org.junit.Assert.assertTrue;

import java.util.UUID;
import java.util.concurrent.Semaphore;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cs.ucc.dc1.cloud.Cloud;
import cs.ucc.dc1.datacentre.Datacentre;
import cs.ucc.dc1.datacentre.Datacentre.CoolingSystemEnum;
import cs.ucc.dc1.datacentre.bean.CPU;
import cs.ucc.dc1.utils.Configuration;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public class CpuTesting extends Application {

	private static CPU cpu;
	private static Semaphore sem = new Semaphore(0);
	private static int NB_OF_TESTS = 8;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Application.launch(CpuTesting.class);
			}
		}).start();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		sem.acquire();
		cpu.setTemperature(Configuration.CPU_MIN_TEMPERATURE);
	}
	
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testUpdateTemperatureRule1() {
		double TEMP = Configuration.CPU_MIN_TEMPERATURE + 10;
		int CPULOAD = 50;
		assertTrue(cpu.getTemperature() == Configuration.CPU_MIN_TEMPERATURE);
		cpu.updateTemperature(CoolingSystemEnum.ON, CPULOAD, TEMP, true);
		assertTrue((cpu.getTemperature() + 0.1d) == TEMP);
	}
	
	@Test
	public void testUpdateTemperatureRule2() {
		double TEMP = Configuration.CPU_MIN_TEMPERATURE + 10;
		int CPULOAD = 50;
		assertTrue(cpu.getTemperature() == Configuration.CPU_MIN_TEMPERATURE);
		cpu.updateTemperature(CoolingSystemEnum.ON, CPULOAD, TEMP, false);
		assertTrue((cpu.getTemperature() + 2.0d) == TEMP);
	}
	
	@Test
	public void testUpdateTemperatureRule3() {
		double TEMP = Configuration.CPU_MIN_TEMPERATURE;
		int CPULOAD = 40;
		assertTrue(cpu.getTemperature() == Configuration.CPU_MIN_TEMPERATURE);
		cpu.updateTemperature(CoolingSystemEnum.ON, CPULOAD, TEMP, true);
		assertTrue(cpu.getTemperature() == Configuration.CPU_MIN_TEMPERATURE);
	}
	
	@Test
	public void testUpdateTemperatureRule4() {
		double TEMP = Configuration.CPU_MIN_TEMPERATURE;
		int CPULOAD = 40;
		assertTrue(cpu.getTemperature() == Configuration.CPU_MIN_TEMPERATURE);
		cpu.updateTemperature(CoolingSystemEnum.ON, CPULOAD, TEMP, false);
		assertTrue(cpu.getTemperature() == Configuration.CPU_MIN_TEMPERATURE);
	}
	
	@Test
	public void testUpdateTemperatureRule5() {
		double TEMP = Configuration.CPU_MIN_TEMPERATURE + 10;
		int CPULOAD = 40;
		assertTrue(cpu.getTemperature() == Configuration.CPU_MIN_TEMPERATURE);
		cpu.updateTemperature(CoolingSystemEnum.OFF, CPULOAD, TEMP, true);
		assertTrue((cpu.getTemperature() - 1d) == TEMP);
		
		CPULOAD = 60;
		cpu.setTemperature(TEMP);
		cpu.updateTemperature(CoolingSystemEnum.OFF, CPULOAD, TEMP, true);
		assertTrue((cpu.getTemperature() - 2d) == TEMP);
		
		CPULOAD = 80;
		cpu.setTemperature(TEMP);
		cpu.updateTemperature(CoolingSystemEnum.OFF, CPULOAD, TEMP, true);
		assertTrue((cpu.getTemperature() - 4d) == TEMP);
	}
	
	@Test
	public void testUpdateTemperatureRule6() {
		double TEMP = Configuration.CPU_MIN_TEMPERATURE + 10;
		int CPULOAD = 40;
		assertTrue(cpu.getTemperature() == Configuration.CPU_MIN_TEMPERATURE);
		cpu.updateTemperature(CoolingSystemEnum.OFF, CPULOAD, TEMP, false);
		assertTrue(cpu.getTemperature() == Configuration.CPU_MIN_TEMPERATURE);
	}
	
	@Test
	public void testUpdateTemperatureRule7() {
		double TEMP = Configuration.CPU_MIN_TEMPERATURE;
		int CPULOAD = 40;
		assertTrue(cpu.getTemperature() == Configuration.CPU_MIN_TEMPERATURE);
		cpu.updateTemperature(CoolingSystemEnum.OFF, CPULOAD, TEMP, true);
		assertTrue((cpu.getTemperature() - 1d) == TEMP);
		
		CPULOAD = 60;
		cpu.setTemperature(TEMP);
		cpu.updateTemperature(CoolingSystemEnum.OFF, CPULOAD, TEMP, true);
		assertTrue((cpu.getTemperature() - 2d) == TEMP);
		
		CPULOAD = 80;
		cpu.setTemperature(TEMP);
		cpu.updateTemperature(CoolingSystemEnum.OFF, CPULOAD, TEMP, true);
		assertTrue((cpu.getTemperature() - 4d) == TEMP);
	}
	
	@Test
	public void testUpdateTemperatureRule8() {
		double TEMP = Configuration.CPU_MIN_TEMPERATURE;
		int CPULOAD = 40;
		assertTrue(cpu.getTemperature() == Configuration.CPU_MIN_TEMPERATURE);
		cpu.updateTemperature(CoolingSystemEnum.OFF, CPULOAD, TEMP, false);
		assertTrue(cpu.getTemperature() == Configuration.CPU_MIN_TEMPERATURE);
	}


	@Override
	public void start(Stage primaryStage) throws Exception {
		Stage stage = new Stage();
		Group root = new Group();
		TextArea console = new TextArea();
		String id = UUID.randomUUID().toString();
		Cloud cloud = new Cloud(null, null, null);
		
		Datacentre datacentre = new Datacentre(stage, root, console, id, cloud);
		datacentre.setCoolingSystemLabel(new Label());
		
		cpu = new CPU(datacentre);
		sem.release(NB_OF_TESTS);
	}
	
}
