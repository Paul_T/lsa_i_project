package test.cs.ucc.dc1;

import java.util.UUID;
import java.util.concurrent.Semaphore;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cs.ucc.dc1.cloud.Cloud;
import cs.ucc.dc1.datacentre.Datacentre;
import cs.ucc.dc1.datacentre.Datacentre.CoolingSystemEnum;
import cs.ucc.dc1.datacentre.bean.Job;
import cs.ucc.dc1.utils.Configuration;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public class DatacentreTesting extends Application {

	private static Datacentre datacentre;
	private static Semaphore sem = new Semaphore(0);
	private static int NB_OF_TESTS = 3;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Application.launch(DatacentreTesting.class);
			}
		}).start();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		sem.acquire();
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testDatacentre() throws InterruptedException {
		assert(datacentre != null);
		assert(datacentre.getId() != null || !datacentre.getId().isEmpty());
		assert(datacentre.getCoolingSystem().equals(CoolingSystemEnum.OFF));
		assert(datacentre.getDatacentreDatabase() != null);
		assert(datacentre.getDatacentreDatabase().getCpus().size() > 0);
		assert(datacentre.getDatacentreDatabase().getCpus().get(0).getTemperature() == Configuration.CPU_MIN_TEMPERATURE);
	}
	
	@Test
	public void testSetCoolingSystem() {
		assert(datacentre.getCoolingSystem().equals(CoolingSystemEnum.OFF));
		datacentre.setCoolingSystem(CoolingSystemEnum.ON);
		assert(datacentre.getCoolingSystem().equals(CoolingSystemEnum.ON));
		datacentre.setCoolingSystem(CoolingSystemEnum.OFF);
		assert(datacentre.getCoolingSystem().equals(CoolingSystemEnum.OFF));
	}

	@Test
	public void testAppendToJobList() {
		assert(datacentre.getWaitingJobList().size() == 0);
		Job job = new Job();
		job.setJobId(UUID.randomUUID().toString());
		datacentre.appendToJobList(job);
		assert(datacentre.getWaitingJobList().size() == 1);
	}
	

	@Override
	public void start(Stage primaryStage) throws Exception {
		Stage stage = new Stage();
		Group root = new Group();
		TextArea console = new TextArea();
		String id = UUID.randomUUID().toString();
		Cloud cloud = new Cloud(null, null, null);
		
		datacentre = new Datacentre(stage, root, console, id, cloud);
		datacentre.setCoolingSystemLabel(new Label());
		sem.release(NB_OF_TESTS);
	}
}
