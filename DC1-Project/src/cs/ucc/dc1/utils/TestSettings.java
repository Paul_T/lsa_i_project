package cs.ucc.dc1.utils;

/** Contains test settinsg (such as number of cpus per datacentre)*/
public abstract class TestSettings {
	
	public static final int NO_DATACENTER = 2;
	public static final int NO_CPU_PER_DATACENTER = 3;
	public static final float MAX_DURATION_JOB = 500;
	public static boolean SKIP_LOAD_BALANCE = false;
}
