package cs.ucc.dc1.datacentre.manager.migration;
/** Critical CPUs will have their jobs migrated away */
public enum CPUStatus {
	critical,
	fine
}
