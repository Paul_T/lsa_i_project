package test.cs.ucc.dc1;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.UUID;
import java.util.concurrent.Semaphore;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cs.ucc.dc1.cloud.Cloud;
import cs.ucc.dc1.datacentre.Datacentre;
import cs.ucc.dc1.datacentre.bean.UserPriorityEnum;
import cs.ucc.dc1.user.UserGUI;
import cs.ucc.dc1.user.UserGUI.ExecutableNameException;
import cs.ucc.dc1.user.UserGUI.JobDurationException;
import cs.ucc.dc1.user.UserGUI.UsernameException;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public class UserGUITesting extends Application {

	private static UserGUI userGUI;
	private static Semaphore sem = new Semaphore(0);
	private static int NB_OF_TESTS = 24;
	private static String USER_V = UserPriorityEnum.values()[0].toString();
	private static String USER_IV = "";
	private static String EXCNAME_V = "myJar.jar";
	private static String EXCNAME_IV = "";
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Application.launch(UserGUITesting.class);
			}
		}).start();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		sem.acquire();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void test1() {
		try {
			userGUI.createJob(0, "0", EXCNAME_V, USER_V);
		} catch (JobDurationException e) {
			assertNotNull(e);
		} catch (ExecutableNameException e) {
			fail("Executable name is supposed to be valid");
		} catch (UsernameException e) {
			fail("Username is supposed to be valid");
		}
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void test2() {
		try {
			userGUI.createJob(0, "0", EXCNAME_V, USER_IV);
		} catch (JobDurationException e) {
			assertNotNull(e);
		} catch (ExecutableNameException e) {
			fail("Executable name is supposed to be valid");
		} catch (UsernameException e) {
			assertNotNull(e);
		}
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void test3() {
		try {
			userGUI.createJob(0, "0", EXCNAME_IV, USER_V);
		} catch (JobDurationException e) {
			assertNotNull(e);
		} catch (ExecutableNameException e) {
			assertNotNull(e);
		} catch (UsernameException e) {
			fail("Username is supposed to be valid");
		}
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void test4() {
		try {
			userGUI.createJob(0, "0", EXCNAME_IV, USER_IV);
		} catch (JobDurationException e) {
			assertNotNull(e);
		} catch (ExecutableNameException e) {
			assertNotNull(e);
		} catch (UsernameException e) {
			assertNotNull(e);
		}
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void test5() {
		try {
			userGUI.createJob(0, "1", EXCNAME_V, USER_V);
		} catch (JobDurationException e) {
			fail("Job duration is supposed to be valid");
		} catch (ExecutableNameException e) {
			fail("Executable name is supposed to be valid");
		} catch (UsernameException e) {
			fail("Username is supposed to be valid");
		}
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void test6() {
		try {
			userGUI.createJob(0, "1", EXCNAME_V, USER_IV);
		} catch (JobDurationException e) {
			fail("Job duration is supposed to be valid");
		} catch (ExecutableNameException e) {
			fail("Executable name is supposed to be valid");
		} catch (UsernameException e) {
			assertNotNull(e);
		}
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void test7() {
		try {
			userGUI.createJob(0, "1", EXCNAME_IV, USER_V);
		} catch (JobDurationException e) {
			fail("Job duration is supposed to be valid");
		} catch (ExecutableNameException e) {
			assertNotNull(e);
		} catch (UsernameException e) {
			fail("Username is supposed to be valid");
		}
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void test8() {
		try {
			userGUI.createJob(0, "1", EXCNAME_IV, USER_IV);
		} catch (JobDurationException e) {
			fail("Job duration is supposed to be valid");
		} catch (ExecutableNameException e) {
			assertNotNull(e);
		} catch (UsernameException e) {
			assertNotNull(e);
		}
	}
	
	@Test(expected = JobDurationException.class)
	public void test9() throws JobDurationException {
		try {
			userGUI.createJob(1, "0", EXCNAME_V, USER_V);
		} catch (ExecutableNameException e) {
			fail("Executable name is supposed to be valid");
		} catch (UsernameException e) {
			fail("Username is supposed to be valid");
		}
	}
	
	@Test(expected = JobDurationException.class)
	public void test10() throws JobDurationException {
		try {
			userGUI.createJob(1, "0", EXCNAME_V, USER_IV);
		} catch (ExecutableNameException e) {
			fail("Executable name is supposed to be valid");
		} catch (UsernameException e) {
			assertNotNull(e);
		}
	}
	
	@Test(expected = JobDurationException.class)
	public void test11() throws JobDurationException {
		try {
			userGUI.createJob(1, "0", EXCNAME_IV, USER_V);
		} catch (ExecutableNameException e) {
			assertNotNull(e);
		} catch (UsernameException e) {
			fail("Username is supposed to be valid");
		}
	}
	
	@Test(expected = JobDurationException.class)
	public void test12() throws JobDurationException {
		try {
			userGUI.createJob(1, "0", EXCNAME_IV, USER_IV);
		} catch (ExecutableNameException e) {
			assertNotNull(e);
		} catch (UsernameException e) {
			assertNotNull(e);
		}
	}
	

	@Test
	public void test13() {
		try {
			userGUI.createJob(1, "1", EXCNAME_V, USER_V);
		} catch (JobDurationException e) {
			fail("Job duration is supposed to be larger than 0 and smaller than 673");
		} catch (ExecutableNameException e) {
			fail("Executable name is supposed to be valid");
		} catch (UsernameException e) {
			fail("Username is supposed to be valid");
		}
	}
	
	@Test
	public void test14() {
		try {
			userGUI.createJob(1, "1",EXCNAME_V, USER_IV);
		} catch (JobDurationException e) {
			fail("Job duration is supposed to be larger than 0 and smaller than 673");
		} catch (ExecutableNameException e) {
			fail("Executable name is supposed to be valid");
		} catch (UsernameException e) {
			assertNotNull(e);
		}
	}
	
	@Test
	public void test15() {
		try {
			userGUI.createJob(1, "1", EXCNAME_IV, USER_V);
		} catch (JobDurationException e) {
			fail("Job duration is supposed to be larger than 0 and smaller than 673");
		} catch (ExecutableNameException e) {
			assertNotNull(e);
		} catch (UsernameException e) {
			fail("Username is supposed to be valid");
		}
	}
	
	@Test
	public void test16() {
		try {
			userGUI.createJob(1, "1", EXCNAME_IV, USER_IV);
		} catch (JobDurationException e) {
			fail("Job duration is supposed to be larger than 0 and smaller than 673");
		} catch (ExecutableNameException e) {
			assertNotNull(e);
		} catch (UsernameException e) {
			assertNotNull(e);
		}
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void test17() {
		try {
			userGUI.createJob(21, "672", EXCNAME_V, USER_V);
		} catch (JobDurationException e) {
			assertNotNull(e);
		} catch (ExecutableNameException e) {
			fail("Executable name is supposed to be valid");
		} catch (UsernameException e) {
			fail("Username is supposed to be valid");
		}
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void test18() {
		try {
			userGUI.createJob(21, "672", EXCNAME_V, USER_IV);
		} catch (JobDurationException e) {
			assertNotNull(e);
		} catch (ExecutableNameException e) {
			fail("Executable name is supposed to be valid");
		} catch (UsernameException e) {
			assertNotNull(e);
		}
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void test19() {
		try {
			userGUI.createJob(21, "672", EXCNAME_IV, USER_V);
		} catch (JobDurationException e) {
			assertNotNull(e);
		} catch (ExecutableNameException e) {
			fail("Executable name is supposed to be valid");
		} catch (UsernameException e) {
			assertNotNull(e);
		}
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void test20() {
		try {
			userGUI.createJob(21, "672", EXCNAME_IV, USER_IV);
		} catch (JobDurationException e) {
			fail("Job duration is supposed to be larger than 0 and smaller than 673");
		} catch (ExecutableNameException e) {
			fail("Executable name is supposed to be valid");
		} catch (UsernameException e) {
			fail("Username is supposed to be valid");
		}
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void test21() {
		try {
			userGUI.createJob(21, "673", EXCNAME_V, USER_V);
		} catch (JobDurationException e) {
			fail("Job duration is supposed to be larger than 0 and smaller than 673");
		} catch (ExecutableNameException e) {
			assertNotNull(e);
		} catch (UsernameException e) {
			assertNotNull(e);
		}
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void test22() {
		try {
			userGUI.createJob(21, "673", EXCNAME_V, USER_IV);
		} catch (JobDurationException e) {
			fail("Job duration is supposed to be larger than 0 and smaller than 673");
		} catch (ExecutableNameException e) {
			fail("Executable name is supposed to be valid");
		} catch (UsernameException e) {
			assertNotNull(e);
		}
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void test23() {
		try {
			userGUI.createJob(21, "673", EXCNAME_IV, USER_V);
		} catch (JobDurationException e) {
			fail("Job duration is supposed to be larger than 0 and smaller than 673");
		} catch (ExecutableNameException e) {
			assertNotNull(e);
		} catch (UsernameException e) {
			fail("Username is supposed to be valid");
		}
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void test24() {
		try {
			userGUI.createJob(21, "673", EXCNAME_IV, USER_IV);
		} catch (JobDurationException e) {
			fail("Job duration is supposed to be larger than 0 and smaller than 673");
		} catch (ExecutableNameException e) {
			assertNotNull(e);
		} catch (UsernameException e) {
			assertNotNull(e);
		}
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		userGUI = new UserGUI(new Stage(), new Group(), new TextArea(), UUID.randomUUID().toString().replaceAll("-", "").substring(0, 7));		
		Cloud cloud = new Cloud(new Stage(), new Group(), new TextArea());
		userGUI.setCloud(cloud);
		cloud.addConnectedDatacentre(new Datacentre(new Stage(), new Group(), 
				new TextArea(), UUID.randomUUID().toString().replaceAll("-", "").substring(0, 7), cloud));
		sem.release(NB_OF_TESTS);
	}
}
