package cs.ucc.dc1.datacentre.manager.migration;

import java.util.ArrayList;
import java.util.List;

import cs.ucc.dc1.datacentre.bean.CPU;
import cs.ucc.dc1.utils.Configuration;

/** used during the Migration process
 */
public class CPUSorter {
	
	/**
	 * Sort cpus by CPU load descending (the highest will be first in the list)
	 * @param cpus cpus needed to be sorted
	 * @return sorted list
	 */
	public List<CPU> sortCPUByCPUUsage(List<CPU> cpus){
		System.out.println("\t\t" + this.getClass().getName());
		
		List<CPU> sortedCPUs = new ArrayList<CPU>();
		int size = cpus.size();
		
		List<CPU> tempCPUlist = new ArrayList<CPU>();
		for(CPU cpu : cpus) {
			if(cpu.getCpuLoad() > Configuration.CPU_MAX_ALLOWED_LOAD || cpu.getTemperature() > Configuration.CPU_MAX_ALLOWED_TEMPERATURE)
				continue;
			tempCPUlist.add(cpu);
		}
 		
		for(int i = 0 ; i < size ; i ++) {
				
			String idMax = getMaxCPULoad(tempCPUlist);
			for(CPU cpu : tempCPUlist) {
				if(cpu.getIdCPU().equals(idMax)) {
					sortedCPUs.add(cpu);
					tempCPUlist.remove(cpu);
					break;
				}
			}
		}
		
		return sortedCPUs;
	}
	
	private String getMaxCPULoad(List<CPU> cpus) {
		
		int maxLoad = 0;
		String id = null;
		for(CPU cpu: cpus) {
			if(maxLoad <= cpu.getCpuLoad()) {
				maxLoad = cpu.getCpuLoad();
				id = cpu.getIdCPU();
			}
		}
		
		return id;
	}
	
}
