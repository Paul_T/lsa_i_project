package cs.ucc.dc1;

import java.util.ArrayList;
import java.util.List;

import cs.ucc.dc1.cloud.Cloud;
import cs.ucc.dc1.cloud.bean.DatacenterStatusEnum;
import cs.ucc.dc1.cloud.bean.DatacentreBean;
import cs.ucc.dc1.datacentre.Datacentre;
import cs.ucc.dc1.user.UserGUI;
import cs.ucc.dc1.user.UserGUI.ExecutableNameException;
import cs.ucc.dc1.user.UserGUI.JobDurationException;
import cs.ucc.dc1.user.UserGUI.UsernameException;
import cs.ucc.dc1.utils.TestSettings;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;


public class DC1Project extends Application {

	private final int WIDTH = 400;
	private final int HEIGHT = 400;
	private Cloud cloud;
	
	private TextArea cloudConsole;
	private List<TextArea> dcConsoles = new ArrayList<>();
	private List<TextArea> CPUConsoles = new ArrayList<>();
	
	public static void main(String[] args) {

		 Application.launch(DC1Project.class, args);  
	}
	 
	@Override
	public void start(Stage cloudStage) {
		Group root = new Group();
		cloudStage.setTitle("Cloud");
		Scene cloudScene = new Scene(root, WIDTH, HEIGHT, Color.CADETBLUE);
		
		cloudConsole = createConsole();
		cloud = new Cloud(cloudStage, root, cloudConsole);
		
		createDcStage(1, WIDTH+650, HEIGHT, 3);
	    createDcStage(2, WIDTH+650, HEIGHT, 3);
	    
		for (int i = 0; i < 2; i++) {
			DatacentreBean dc = cloud.getDatacentreBean(i);
			Rectangle DC = new Rectangle();
			DC.setWidth(100);
		    DC.setHeight(150);
		    DC.setLayoutX(20 + i*110);
		    DC.setLayoutY(20);
		    DC.setStroke(Color.BLACK);
		    DC.setStrokeWidth(3);
		    DC.setFill(Color.TRANSPARENT);
		    
		    Label dcID = new Label("DC " + (i+1));
		    dcID.setLayoutX(55 + i*110);
		    dcID.setLayoutY(30);
		    
		    Label dcStatus = new Label("UNRESTRICTED");
		    dcStatus.setLayoutX(30 + i*110);
		    dcStatus.setLayoutY(80);
		    
		    Label dcWorkload = new Label("0%");
		    dcWorkload.setLayoutX(65 + i*110);
		    dcWorkload.setLayoutY(130);
	  
		    dc.setStatusLabel(dcStatus);
		    dc.setWorkloadLabel(dcWorkload);
		    
		    root.getChildren().add(dcID);
		    root.getChildren().add(dcStatus);
		    root.getChildren().add(dcWorkload);
	        root.getChildren().add(DC);
		}
		
		Label titleConsole = new Label("Cloud console");
		titleConsole.setLayoutX(20);
		titleConsole.setLayoutY(200);
		root.getChildren().add(titleConsole);
		
		root.getChildren().add(cloudConsole);
		cloudStage.setScene(cloudScene);
		cloudStage.show();
		
	    createUserStage(WIDTH, HEIGHT);
	}
	 
	private void createDcStage(int id, int width, int height, int noCPU) {
		Stage dcStage = new Stage();
		Group root = new Group();
		dcStage.setScene(new Scene(root, width, height, Color.WHITE)); 
		dcStage.setTitle("Datacentre " + id);
		
		TextArea consoleDC = createConsole();
		consoleDC.setLayoutY(250);
		consoleDC.setMaxWidth(1000);
		dcConsoles.add(consoleDC);

		Datacentre dc = new Datacentre(dcStage, root, consoleDC, String.valueOf(id),this.cloud);

		for (int i = 0; i < noCPU; i++) {
			Rectangle CPU = new Rectangle();
		    CPU.setWidth(300);
		    CPU.setHeight(200);
		    CPU.setLayoutX(20 + i*350);
		    CPU.setLayoutY(20);
		    CPU.setStroke(Color.BLACK);
		    CPU.setStrokeWidth(3);
		    CPU.setFill(Color.TRANSPARENT);
		    
		    Label cpuIdLabel = new Label("CPU");
		    cpuIdLabel.setLayoutX(150 + i*350);
		    cpuIdLabel.setLayoutY(30);
		    
		    Label cpuLoadLabel = new Label("Workload");
		    cpuLoadLabel.setLayoutX(130 + i*350);
		    cpuLoadLabel.setLayoutY(45);
		    
		    Label cpuTempLabel = new Label("Temperature");
		    cpuTempLabel.setLayoutX(110 + i*350);
		    cpuTempLabel.setLayoutY(60);
	  
		    root.getChildren().add(cpuIdLabel);
		    root.getChildren().add(cpuLoadLabel);
		    root.getChildren().add(cpuTempLabel);
		    
		    TextArea consoleCPU = createConsole();
		    consoleCPU.setMaxWidth(280);
		    consoleCPU.setLayoutX(30 + i*350);
		    consoleCPU.setLayoutY(80);
		    CPUConsoles.add(consoleCPU);
		        
	        root.getChildren().add(CPU);
	        root.getChildren().add(consoleCPU);
	        
	        dc.setupLabelsForCpu(i, cpuIdLabel, cpuLoadLabel, cpuTempLabel, consoleCPU);
		}
	        
		Label titleConsole = new Label("Datacentre console");
		titleConsole.setLayoutX(20);
		titleConsole.setLayoutY(230);
		
		Label dcCoolingSystemLabel = new Label("Cooling System: ");
		dcCoolingSystemLabel.setLayoutX(390);
		dcCoolingSystemLabel.setLayoutY(230);
		dc.setCoolingSystemLabel(dcCoolingSystemLabel);
		
		root.getChildren().add(titleConsole);
		root.getChildren().add(dcCoolingSystemLabel);
		
		root.getChildren().add(consoleDC);
		dcStage.show();
		DatacentreBean datacentreBean = new DatacentreBean();
		datacentreBean.setId(String.valueOf(id));
		datacentreBean.setWorkload(0);
		datacentreBean.setStatus(DatacenterStatusEnum.UNRESTRICTED);
		
		cloud.addDatacentreBean(datacentreBean);
		cloud.addConnectedDatacentre(dc);
	}
	 
	private void createUserStage(int width, int height) {
		Stage userStage = new Stage();
		Group root = new Group();
		userStage.setScene(new Scene(root, width, height, Color.LIGHTGREEN)); 
		userStage.setTitle("User GUI");
		
		// username
		TextField usernameTf = new TextField();
		usernameTf.setPromptText("Enter the user name");
		usernameTf.setLayoutX(120);
		usernameTf.setLayoutY(0);
		
		// jar file
		TextField executableNameTf = new TextField();
		executableNameTf.setPromptText("Enter the executable");
		executableNameTf.setLayoutX(90);
		executableNameTf.setLayoutY(30);
		
		// duration
		TextField jobDurationTf = new TextField();
		jobDurationTf.setPromptText("Enter the job duration");
		jobDurationTf.setLayoutX(120);
		jobDurationTf.setLayoutY(60);
		jobDurationTf.textProperty().addListener(new ChangeListener<String>() {
		    @Override
		    public void changed(ObservableValue<? extends String> observable, String oldValue, 
		        String newValue) {
		        if (!newValue.matches("\\d*")) {
		        	jobDurationTf.setText(newValue.replaceAll("[^\\d]", ""));
		        }
		    }
		});
		
		// environment
		final ComboBox<String> environmentComboBox = new ComboBox<String>();
		environmentComboBox.getItems().addAll(
            "Tomcat",
            "GlassFish",
            "JBoss",
            "WebSphere",
            "NodeJS"  
        );
		environmentComboBox.setPromptText("Environment");
		environmentComboBox.setLayoutX(240);
		environmentComboBox.setLayoutY(30);
		
		Label titleConsole = new Label("User console");
		titleConsole.setLayoutX(20);
		titleConsole.setLayoutY(200);
		
		Button submit = new Button();
		submit.setText("Submit task");
		submit.setLayoutX(120);
		submit.setLayoutY(90);
		
		CheckBox skipLoadBalanceCb = new CheckBox("Skip load balance");
		skipLoadBalanceCb.setLayoutX(120);
		skipLoadBalanceCb.setLayoutY(150);
		
		submit.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				int environment = environmentComboBox.getSelectionModel().getSelectedIndex() + 1;
				TestSettings.SKIP_LOAD_BALANCE = skipLoadBalanceCb.isSelected();
				try {
					cloud.getUserGUI(0).createJob(environment, jobDurationTf.getText(), executableNameTf.getText(), usernameTf.getText());
				} catch (ExecutableNameException | UsernameException | JobDurationException e) {
					e.printStackTrace();
				}
			}
	    });
		
		TextArea consoleUser = createConsole();
		
		Button clear = new Button();
		clear.setText("Clear");
		clear.setLayoutX(120);
		clear.setLayoutY(120);
		clear.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				consoleUser.clear();
				cloudConsole.clear();
				for (TextArea console : dcConsoles) {
					console.clear();
				}
				for (TextArea console : CPUConsoles) {
					console.clear();
				}
			}
		});
		
		root.getChildren().add(usernameTf);
		root.getChildren().add(executableNameTf);
		root.getChildren().add(environmentComboBox);
		root.getChildren().add(jobDurationTf);
		root.getChildren().add(submit);
		root.getChildren().add(clear);
		root.getChildren().add(skipLoadBalanceCb);
		
		root.getChildren().add(titleConsole);
		
		root.getChildren().add(consoleUser);
		
		userStage.show();
		UserGUI user = new UserGUI(userStage, root, consoleUser, "1");
		user.setCloud(cloud);
		cloud.addUserGUI(user);
	}
	
	private TextArea createConsole() {
		TextArea console = new TextArea();
		console.textProperty().addListener(new ChangeListener<Object>() {
		    @Override
		    public void changed(ObservableValue<?> observable, Object oldValue,
		            Object newValue) {
		    	console.setScrollTop(0); //this will scroll to the bottom
		    }
		});
		
		console.setPrefRowCount(5);            
		console.setEditable(false);
		console.setWrapText(true);
		console.setLayoutX(20);
		console.setLayoutY(220);
		console.setMaxWidth(WIDTH - 50);
		return console;
	}
	
	@Override
	public void stop() throws Exception {
		super.stop();
		System.exit(0);
	}
}
