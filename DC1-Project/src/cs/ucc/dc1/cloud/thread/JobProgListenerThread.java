package cs.ucc.dc1.cloud.thread;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.codehaus.jackson.map.ObjectMapper;

import cs.ucc.dc1.cloud.Cloud;
import cs.ucc.dc1.datacentre.bean.Job;

public class JobProgListenerThread extends Thread {
	
	private BlockingQueue<String> incomingDcJobProgressionPackets = new LinkedBlockingQueue<String>();
	private boolean running;
	private Cloud cloud;
	
	public JobProgListenerThread(Cloud cloud) {
		this.cloud = cloud;
	}
	
	@Override
	public void run() {
		while (running) {
			try {
				String jobAsString = incomingDcJobProgressionPackets.take();
				ObjectMapper mapper = new ObjectMapper();
				Job job = mapper.readValue(jobAsString, Job.class);
				
				if(job.getProgressionAsPercentage() > 0)
					cloud.writeIntoConsole("Job progression received: (" + job.getJobId() + ") " + job.getName() + " Prog: " + job.getProgressionAsPercentage() + "%");
				else {
					cloud.writeIntoConsole("Job (" + job.getJobId() + ") : execution over");
					cloud.getDatacentreById(job.getDatacentreID()).decreaseWorkload(job.getCpuLoad());
				}
			} catch (InterruptedException | IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setRunning(boolean running) {
		this.running = running;
	}
	
	public void pushJob(String jobAsString) {
		try {
			incomingDcJobProgressionPackets.put(jobAsString);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
