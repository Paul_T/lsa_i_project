package cs.ucc.dc1.datacentre.manager.migration;

import java.util.List;

import cs.ucc.dc1.datacentre.bean.CPU;
import cs.ucc.dc1.utils.Configuration;

/** changes the status of the CPU according to its average use and its temperature */
public class CPUStatusMonitor {
	
	public void updateStatusOfCPUs(List<CPU> cpus) {
		System.out.println("\t\t" + this.getClass().getName());
		
		for(CPU cpu : cpus) {
		
			if(cpu.getCpuHistoricalUse() > Configuration.CPU_MAX_ALLOWED_AVERAGE_LOAD 
					|| cpu.getTemperature() > Configuration.CPU_MAX_ALLOWED_TEMPERATURE)
				cpu.setCpuStatus(CPUStatus.critical);
			else
				cpu.setCpuStatus(CPUStatus.fine);
		}
	}
	
	public Boolean hasAnyCriticalCPU(List<CPU> cpus) {
		
		for(CPU cpu : cpus ) {
			if(cpu.getCpuStatus().equals(CPUStatus.critical) && cpu.getCpuLoad() > 0)
				return true;
		}
		return false;
	}

}
