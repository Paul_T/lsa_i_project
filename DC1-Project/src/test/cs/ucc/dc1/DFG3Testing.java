package test.cs.ucc.dc1;


import java.util.UUID;
import java.util.concurrent.Semaphore;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cs.ucc.dc1.cloud.Cloud;
import cs.ucc.dc1.datacentre.Datacentre;
import cs.ucc.dc1.datacentre.bean.CPU;
import cs.ucc.dc1.datacentre.bean.Job;
import cs.ucc.dc1.datacentre.manager.Migrator;
import cs.ucc.dc1.datacentre.manager.Scheduler;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public class DFG3Testing extends Application {

	private static Datacentre datacentre;
	private static Semaphore sem = new Semaphore(0);
	private static int NB_OF_TESTS = 20; // TODO TO BE MODIFIED!

	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Application.launch(DFG3Testing.class);
			}
		}).start();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		datacentre = new Datacentre();
		sem.acquire();
	}

	@After
	public void tearDown() throws Exception {
	}

	
	private void setupLabelsCpu(CPU cpu) {
		cpu.setTempLabel(new Label());
		cpu.setConsoleCPU(new TextArea());
		cpu.setWorkloadLabel(new Label());
		cpu.setIdLabel(new Label());
	}

	@Test
	public void test1() {
		
		boolean isMigrationWorthIt = true;
		Scheduler scheduler = new Scheduler();
		Migrator migrator = new Migrator();
		Boolean release = true;
		
		CPU cpu = new CPU(datacentre);
		setupLabelsCpu(cpu);
		cpu.getJobs().add(new Job());
		datacentre.getDatacentreDatabase().getCpus().add(cpu);
		
		datacentre.performMigration(isMigrationWorthIt, scheduler, migrator, true, release);
	}
	
	@Test
	public void test2() {
		
		boolean isMigrationWorthIt = true;
		Scheduler scheduler = new Scheduler();
		Migrator migrator = new Migrator();
		Boolean release = false;
		
		CPU cpu = new CPU(datacentre);
		setupLabelsCpu(cpu);
		cpu.getJobs().add(new Job());
		datacentre.getDatacentreDatabase().getCpus().add(cpu);
		
		datacentre.performMigration(isMigrationWorthIt, scheduler, migrator, true, release);
	}
	
	@Test
	public void test3() {
		
		boolean isMigrationWorthIt = false;
		Scheduler scheduler = new Scheduler();
		Migrator migrator = new Migrator();
		Boolean release = false;
		
		CPU cpu = new CPU(datacentre);
		setupLabelsCpu(cpu);
		cpu.getJobs().add(new Job());
		datacentre.getDatacentreDatabase().getCpus().add(cpu);
		
		datacentre.performMigration(isMigrationWorthIt, scheduler, migrator, true, release);
	}
	
	@Test
	public void test4() {
		
		boolean isMigrationWorthIt = true;
		Scheduler scheduler = new Scheduler();
		Migrator migrator = new Migrator();
		Boolean release = false;
		
		datacentre.performMigration(isMigrationWorthIt, scheduler, migrator, true, release);
	}
	
	@Test
	public void test5() {
		
		boolean isMigrationWorthIt = true;
		Scheduler scheduler = new Scheduler();
		Migrator migrator = new Migrator();
		Boolean release = true;
		
		datacentre.performMigration(isMigrationWorthIt, scheduler, migrator, true, release);
	}
	
	@Test
	public void test6() {
		
		boolean isMigrationWorthIt = true;
		Scheduler scheduler = new Scheduler();
		Migrator migrator = new Migrator();
		Boolean release = true;
		
		CPU cpu = new CPU(datacentre);
		setupLabelsCpu(cpu);
		datacentre.getDatacentreDatabase().getCpus().add(cpu);
		
		datacentre.performMigration(isMigrationWorthIt, scheduler, migrator, true, release);
	}
	
	@Test
	public void test7() {
		
		boolean isMigrationWorthIt = true;
		Scheduler scheduler = new Scheduler();
		Migrator migrator = new Migrator();
		Boolean release = false;
		
		CPU cpu = new CPU(datacentre);
		setupLabelsCpu(cpu);
		datacentre.getDatacentreDatabase().getCpus().add(cpu);
		
		datacentre.performMigration(isMigrationWorthIt, scheduler, migrator, true, release);
	}
	
	@Test
	public void test8() {
		
		boolean isMigrationWorthIt = true;
		Scheduler scheduler = new Scheduler();
		Migrator migrator = new Migrator();
		Boolean release = true;
		
		CPU cpu = new CPU(datacentre);
		CPU cpu2 = new CPU(datacentre);
		cpu.assignJob(new Job());
		
		datacentre.getDatacentreDatabase().getCpus().add(cpu);
		datacentre.getDatacentreDatabase().getCpus().add(cpu2);
		
		datacentre.performMigration(isMigrationWorthIt, scheduler, migrator, true, release);
	}
	
	@Test
	public void test9() {
		
		boolean isMigrationWorthIt = true;
		Scheduler scheduler = new Scheduler();
		Migrator migrator = new Migrator();
		Boolean release = false;
		
		CPU cpu = new CPU(datacentre);
		setupLabelsCpu(cpu);
		CPU cpu2 = new CPU(datacentre);
		setupLabelsCpu(cpu2);
		cpu.assignJob(new Job());
		
		datacentre.getDatacentreDatabase().getCpus().add(cpu);
		datacentre.getDatacentreDatabase().getCpus().add(cpu2);
		
		datacentre.performMigration(isMigrationWorthIt, scheduler, migrator, true, release);
	}
	
	@Test
	public void test10() {
		
		boolean isMigrationWorthIt = true;
		Scheduler scheduler = new Scheduler();
		Migrator migrator = new Migrator();
		Boolean release = true;
		
		CPU cpu = new CPU(datacentre);
		CPU cpu2 = new CPU(datacentre);
		cpu2.assignJob(new Job());
		
		datacentre.getDatacentreDatabase().getCpus().add(cpu);
		datacentre.getDatacentreDatabase().getCpus().add(cpu2);
		
		datacentre.performMigration(isMigrationWorthIt, scheduler, migrator, true, release);
	}
	
	@Test
	public void test11() {
		
		boolean isMigrationWorthIt = true;
		Scheduler scheduler = new Scheduler();
		Migrator migrator = new Migrator();
		Boolean release = false;
		
		CPU cpu = new CPU(datacentre);
		CPU cpu2 = new CPU(datacentre);
		cpu2.assignJob(new Job());
		
		datacentre.getDatacentreDatabase().getCpus().add(cpu);
		datacentre.getDatacentreDatabase().getCpus().add(cpu2);
		
		datacentre.performMigration(isMigrationWorthIt, scheduler, migrator, true, release);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		Cloud cloud = new Cloud(new Stage(), new Group(), new TextArea());
		datacentre = new Datacentre(new Stage(), new Group(),new TextArea(), UUID.randomUUID().toString().replaceAll("-", "").substring(0, 7), cloud);
		datacentre.setCoolingSystemLabel(new Label());
		sem.release(NB_OF_TESTS);
	}

}
