package cs.ucc.dc1.datacentre.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

import cs.ucc.dc1.datacentre.Datacentre.CoolingSystemEnum;
import cs.ucc.dc1.datacentre.bean.CPU;
import cs.ucc.dc1.datacentre.bean.Job;
import cs.ucc.dc1.utils.Configuration;

/** Simulates the execution of jobs */
public class CPUThread extends Thread{

	private CPU cpu;
	private boolean isRunning = false;
	private Semaphore semaphore;
	
	public CPUThread(CPU cpu) {
		this.cpu = cpu;
		semaphore = new Semaphore(0,true);
	}
	
	@Override
	public void run() {
		
		List<Job> jobs = new ArrayList<Job>();
		
		while(isRunning) {
			
			try {
				semaphore.acquire();
				semaphore.drainPermits();
			
				jobs.addAll(cpu.getJobs());
				
				if (jobs.isEmpty()) {
					cpu.updateTemperature(cpu.getDatacentre().getCoolingSystem(), cpu.getCpuLoad(), cpu.getTemperature(),false);
					sleep();
				} else {
				
					for(Job job : jobs) {
						job.decreaseRemainingExecutionTime();
						
						cpu.sendProgressionToCloud(job);
						if(job.getRemainingExecutionTime() == 0) {
							cpu.endJob(job);
							cpu.updateLabels();
						}
					}
					cpu.updateTemperature(cpu.getDatacentre().getCoolingSystem(), cpu.getCpuLoad(), cpu.getTemperature(),true);
					sleep();
				}
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			jobs.clear();
			
			// If there are jobs to do OR Cooling system ON with this CPU hot
			if ((cpu.getJobs().size() > 0) || 
					(cpu.getTemperature() > Configuration.CPU_MIN_TEMPERATURE) && 
					(cpu.getDatacentre().getCoolingSystem().equals(CoolingSystemEnum.ON))) {
				semaphore.release();
			}
		}
	}

	private void sleep() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void release() {
		semaphore.release();
	}
	
	public void setIsRunning(Boolean isRunning) {
		this.isRunning = isRunning;
	}
}
