package cs.ucc.dc1.utils;

/**	Contains static configuration values 
 * @author mattp
 */
public abstract class Configuration {
	
	public static final double CPU_MIN_TEMPERATURE = 40.0;
	public static final double CPU_MAX_ALLOWED_TEMPERATURE = 75.0;
	public static final double CPU_TRESHOLD_COOLING_SYSTEM = 80.0;
	public static final double CPU_MAX_ALLOWED_AVERAGE_LOAD = 60.0;
	public static final double CPU_MAX_ALLOWED_LOAD = 90.0;
	public static final double CPU_LIMIT_LOWEST_VALUE = 50;
	
	public static final double CPU_MAX_LOAD = 100.0;
	
	public static final double DC_MAX_ALLOWED_LOAD = (TestSettings.NO_CPU_PER_DATACENTER * CPU_MAX_LOAD);
	public static final double DC_TRIGGER_ASSIGNMENT_AND_MIGRATION = 1;
	
	public static final double DC_THRESHOLD_LIMITED_STATUS = (DC_MAX_ALLOWED_LOAD - 0.2*DC_MAX_ALLOWED_LOAD);

	public static final int CLOUD_TRIGGER_LOAD_BALANCING = 1;
}
