package cs.ucc.dc1.datacentre.thread;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.codehaus.jackson.map.ObjectMapper;

import cs.ucc.dc1.datacentre.Datacentre;
import cs.ucc.dc1.datacentre.bean.Job;

/** Receives messages coming from the cloud */
public class CloudListenerThread extends Thread{
	private BlockingQueue<String> incomingCloudPackets = new LinkedBlockingQueue<String>();
	private boolean running;
	private Datacentre datacentre;
	
	public CloudListenerThread(Datacentre datacentre) {
		this.datacentre = datacentre;
	}
	
	@Override
	public void run() {
		while (running) {
			try {
				String jobAsString = incomingCloudPackets.take();
				ObjectMapper mapper = new ObjectMapper();
				Job job = mapper.readValue(jobAsString, Job.class);
				datacentre.writeIntoConsole("Job received: (" + job.getJobId() + ") " + job.getName() + " : Priority " + job.getPriority());
				datacentre.appendToJobList(job);
			} catch (InterruptedException | IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setRunning(boolean running) {
		this.running = running;
	}
	
	public void pushJob(String jobAsString) {
		try {
			incomingCloudPackets.put(jobAsString);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
