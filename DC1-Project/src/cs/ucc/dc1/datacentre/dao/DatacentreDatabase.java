package cs.ucc.dc1.datacentre.dao;

import java.util.ArrayList;
import java.util.List;

import cs.ucc.dc1.datacentre.Datacentre;
import cs.ucc.dc1.datacentre.bean.CPU;
import cs.ucc.dc1.datacentre.bean.Job;

/** Database inside a datacentre storing all the cpus of this datacentre*/
public class DatacentreDatabase {
	
	private ArrayList<CPU> cpus = new ArrayList<CPU>();

	public DatacentreDatabase(int no_cpus, Datacentre datacentre) {
		
		for(int i = 0 ; i < no_cpus ; i++) {
			cpus.add(new CPU(datacentre));
		}
		
	}

	public ArrayList<CPU> getCpus() {
		return cpus;
	}

	public void setCpus(ArrayList<CPU> cpus) {
		this.cpus = cpus;
	}

	/**
	 * Get all jobs currently running in the DC
	 * @return list of running jobs
	 */
	public List<Job> getJobsFromDatabase(){
		List<Job> jobs = new ArrayList<Job>();
		for(CPU cpu : cpus) {
			jobs.addAll(cpu.getJobs());
		}
		return jobs;
	}
	
	
}
