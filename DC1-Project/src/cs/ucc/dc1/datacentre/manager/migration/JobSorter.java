package cs.ucc.dc1.datacentre.manager.migration;

import java.util.ArrayList;
import java.util.List;

import cs.ucc.dc1.datacentre.bean.CPU;
import cs.ucc.dc1.datacentre.bean.Job;

/**
 * Sort jobs
 *
 */
public class JobSorter {
	
	/**
	 * First puts the critical jobs, then the fine jobs
	 * @param cpus all cpus from the system
	 * @return list of jobs, with the ones that MUST be migrated first
	 */
	public List<Job> sortJobsByOwnerLoad(List<CPU> cpus, String idCPU) {
		System.out.println("\t\t" + this.getClass().getName());
		
		List<Job> fineJobs = new ArrayList<Job>();
		List<Job> criticalJobs = new ArrayList<Job>();
		
		for(CPU cpu : cpus) {
			if(cpu.getIdCPU().equals(idCPU))
				continue;
			
			for(Job job : cpu.getJobs()) {	
				if(cpu.getCpuStatus().equals(CPUStatus.critical))
					criticalJobs.add(job);
				else
					fineJobs.add(job);
			}
		}
		
		List<Job> finalSortedJobs = new ArrayList<Job>();
		finalSortedJobs.addAll(this.sortJobByCPULoadDescending(criticalJobs));
		finalSortedJobs.addAll(this.sortJobByCPULoadDescending(fineJobs));
		
		return finalSortedJobs;
	}
	
	private List<Job> sortJobByCPULoadDescending(List<Job> jobs) {
		List<Job> sortedJobs = new ArrayList<Job>();
		
		int size = jobs.size(); 
		for(int i = 0 ; i < size; i++) {
			
			String maxCPULoadId = getJobIDWithMaxLoad(jobs);
			for(Job job : jobs) {
				if(job.getJobId().equals(maxCPULoadId)) {
					sortedJobs.add(job);
					jobs.remove(job);
					break;
				}
			}
		}
		
		return sortedJobs;
	}
	
	private String getJobIDWithMaxLoad(List<Job> jobs) {
		
		int maxLoad = 0;
		String id = null;
		for(Job job: jobs) {
			if(maxLoad < job.getCpuLoad()) {
				maxLoad = job.getCpuLoad();
				id = job.getJobId();
			}
		}
		
		return id;
	}

}
