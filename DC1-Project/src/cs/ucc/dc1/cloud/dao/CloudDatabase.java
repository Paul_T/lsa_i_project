package cs.ucc.dc1.cloud.dao;

import java.util.ArrayList;
import java.util.List;

import cs.ucc.dc1.datacentre.Datacentre;


public class CloudDatabase {
	
	private List<Datacentre> connectedDcList = new ArrayList<>();
	

	public CloudDatabase() {
		
	}

	public List<Datacentre> getConnectedDcList() {
		return connectedDcList;
	}

	public void setConnectedDcList(List<Datacentre> connectedDcList) {
		this.connectedDcList = connectedDcList;
	}	
}
