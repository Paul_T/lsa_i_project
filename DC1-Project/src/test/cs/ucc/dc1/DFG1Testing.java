package test.cs.ucc.dc1;

import static org.junit.Assert.fail;

import java.util.concurrent.Semaphore;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cs.ucc.dc1.cloud.Cloud;
import cs.ucc.dc1.cloud.bean.DatacenterStatusEnum;
import cs.ucc.dc1.cloud.bean.DatacentreBean;
import cs.ucc.dc1.datacentre.bean.Job;
import cs.ucc.dc1.utils.TestSettings;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public class DFG1Testing extends Application {

	private static Cloud cloud;
	private static Semaphore sem = new Semaphore(0);
	private static int NB_OF_TESTS = 2;

	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Application.launch(DFG1Testing.class);
			}
		}).start();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		sem.acquire();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test1() {
		TestSettings.SKIP_LOAD_BALANCE = true;
		
		DatacentreBean dc = new DatacentreBean();
		Job job = new Job();
		job.setCpuLoad(40);
		
		cloud.getDcList().add(dc);
		cloud.getWaitingJobList().add(job);
		
		try {
			cloud.startLoadBalancing();
			
			// if true, it means that the job was correctly balanced
			assert(dc.getWorkload() == 40);
		} catch(Exception e) {
			fail("error");
		}
		assert(true);
	}
	
	@Test
	public void test2() {
		TestSettings.SKIP_LOAD_BALANCE = false;
		
		DatacentreBean dc = new DatacentreBean();
		dc.setStatus(DatacenterStatusEnum.UNRESTRICTED);
		Job job = new Job();
		job.setCpuLoad(40);
		
		cloud.getDcList().add(dc);
		cloud.getWaitingJobList().add(job);
		
		try {
			cloud.startLoadBalancing();
			
			// if true, it means that the job was correctly balanced
			assert(dc.getWorkload() == 40);
		} catch(Exception e) {
			fail("error");
		}
		assert(true);
	}

	@Override
	public void start(Stage arg0) throws Exception {
		cloud = new Cloud(new Stage(), new Group(), new TextArea());
		sem.release(NB_OF_TESTS);
	}

}
