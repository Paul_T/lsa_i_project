package cs.ucc.dc1.cloud.manager.loadbalancer;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import cs.ucc.dc1.cloud.Cloud;
import cs.ucc.dc1.datacentre.bean.Job;
import cs.ucc.dc1.utils.Pair;

public class JobAssignor {

	public void associateJobs(Cloud cloud, List<Pair<String, Job>> associations) {
		System.out.println("\t\t" + this.getClass().getName());
		ObjectMapper mapper = new ObjectMapper();
		for (Pair<String, Job> asso : associations) {
			try {
				System.out.println("\t\t" + this.getClass().getName() + " - Routing");
				cloud.simulateJobRouting(mapper.writeValueAsString(asso.getSecond()), asso.getFirst());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
