package cs.ucc.dc1.cloud.thread;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.codehaus.jackson.map.ObjectMapper;

import cs.ucc.dc1.cloud.Cloud;
import cs.ucc.dc1.datacentre.bean.Job;

public class UserListenerThread extends Thread {

	private BlockingQueue<String> incomingUserPackets = new LinkedBlockingQueue<String>();
	private boolean running;
	private Cloud cloud;
	
	public UserListenerThread(Cloud cloud) {
		this.cloud = cloud;
	}
	
	@Override
	public void run() {
		while (running) {
			try {
				String jobAsString = incomingUserPackets.take();
				ObjectMapper mapper = new ObjectMapper();
				Job job = mapper.readValue(jobAsString, Job.class);
				cloud.writeIntoConsole("Job received: (" + job.getJobId() + ") " + job.getName());
				cloud.appendToJobList(job);
			} catch (InterruptedException | IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setRunning(boolean running) {
		this.running = running;
	}
	
	public void pushJob(String jobAsString) {
		try {
			incomingUserPackets.put(jobAsString);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
