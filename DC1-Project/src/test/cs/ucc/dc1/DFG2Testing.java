package test.cs.ucc.dc1;

import java.util.UUID;
import java.util.concurrent.Semaphore;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cs.ucc.dc1.cloud.Cloud;
import cs.ucc.dc1.datacentre.Datacentre;
import cs.ucc.dc1.datacentre.bean.Job;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public class DFG2Testing extends Application {
	
	private static Datacentre datacentre;
	private static Semaphore sem = new Semaphore(0);
	private static int NB_OF_TESTS = 20; // TODO TO BE MODIFIED!
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Application.launch(DFG2Testing.class);
			}
		}).start();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		sem.acquire();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test1() {
		Job job = new Job();
		job.setJobId("testID");
		datacentre = new Datacentre();
		
		datacentre.getNewlyArrivedJobs().add(job);
		datacentre.assignAndMigrate();
		
		// check if the job has been successfully assigned
		assert(datacentre.getDatacentreDatabase().getCpus().get(0).getJobs().get(0).equals(job));
	}
	
	@Test
	public void test2() {
		datacentre = new Datacentre();

		for(int i = 0 ; i < 12 ; i++) {
			Job job = new Job();
			job.setCpuLoad(20);
			job.setRemainingExecutionTime(40);
			datacentre.getNewlyArrivedJobs().add(job);
			datacentre.assignAndMigrate();
		}
		
		Job job = new Job();
		job.setJobId("sampleJOB");
		job.setCpuLoad(30);
		
		datacentre.getNewlyArrivedJobs().add(job);
		datacentre.assignAndMigrate();
		
		// Migration has been triggered by the sampleJob
		assert(datacentre.getDatacentreDatabase().getCpus().get(0).getCpuLoad() == 40);
		assert(datacentre.getDatacentreDatabase().getCpus().get(1).getCpuLoad() == 100);
		assert(datacentre.getDatacentreDatabase().getCpus().get(2).getCpuLoad() == 100);
	}
	
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		Cloud cloud = new Cloud(new Stage(), new Group(), new TextArea());
		datacentre = new Datacentre(new Stage(), new Group(),new TextArea(), UUID.randomUUID().toString().replaceAll("-", "").substring(0, 7), cloud);
		datacentre.setCoolingSystemLabel(new Label());
		sem.release(NB_OF_TESTS);
	}

}
