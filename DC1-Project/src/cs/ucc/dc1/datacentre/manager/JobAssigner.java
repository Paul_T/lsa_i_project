package cs.ucc.dc1.datacentre.manager;

import cs.ucc.dc1.datacentre.Datacentre;
import cs.ucc.dc1.datacentre.bean.CPU;
import cs.ucc.dc1.datacentre.bean.Job;
import cs.ucc.dc1.utils.Configuration;

/** Assigns a job to a cpu when the job arrives in the datacentre */
public class JobAssigner {

	private Datacentre datacentre;
	
	public JobAssigner(Datacentre datacentre) {
		this.datacentre = datacentre;
	}
	
	/**
	 * Assigns the job to the first valid CPU
	 * @param job job that is assigned to a CPU
	 */
	public Boolean assignToFirstValidCPU(Job job) {
		
		System.out.println("\t" + this.getClass().getName() + " : Job (" + job.getJobId() + ")");
		for(CPU cpu : datacentre.getDatacentreDatabase().getCpus()) {
			if(cpu.getCpuHistoricalUse() > Configuration.CPU_MAX_ALLOWED_AVERAGE_LOAD 
					|| cpu.getTemperature() > Configuration.CPU_MAX_ALLOWED_TEMPERATURE
					|| (cpu.getCpuLoad() + job.getCpuLoad()) > Configuration.CPU_MAX_ALLOWED_LOAD)
				continue;
			
			cpu.assignJob(job);
			return true;
		}
		return false;
	}
}
