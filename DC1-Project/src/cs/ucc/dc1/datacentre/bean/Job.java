package cs.ucc.dc1.datacentre.bean;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.UUID;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.ObjectMapper;

/** 
 * Task performed in a CPU 
 * <br> The job is first created by the user, then sent to the cloud. The cloud routes it to a Datacentre. The Datacentre determines a 
 * CPU that can execute it. During its execution, the user will receive message about the progress of the execution of the job.
 */
public class Job {

	private float remainingExecutionTime;
	private float startingExecutionTime;
	private String name;
	private int priority;
	private int cpuLoad;
	private String environment;
	private String userId;
	private String jobId;
	
	private String datacentreID;
	
	public Job() {
		this.jobId = UUID.randomUUID().toString().replaceAll("-", "").substring(0,7);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getPriority() {
		return priority;
	}
	
	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	public int getCpuLoad() {
		return cpuLoad;
	}
	
	public void setCpuLoad(int cpuLoad) {
		this.cpuLoad = cpuLoad;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	
	public float getRemainingExecutionTime() {
		return remainingExecutionTime;
	}

	public void setRemainingExecutionTime(float remainingExecutionTime) {
		this.remainingExecutionTime = remainingExecutionTime;
	}

	public float getStartingExecutionTime() {
		return startingExecutionTime;
	}

	public void setStartingExecutionTime(float statingExecutionTime) {
		this.startingExecutionTime = statingExecutionTime;
	}

	public String getDatacentreID() {
		return datacentreID;
	}

	public void setDatacentreID(String datacentreID) {
		this.datacentreID = datacentreID;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	/** @return progression as percentage with 2 digits after . */
	@JsonIgnore
	public float getProgressionAsPercentage() {
		return new BigDecimal((this.remainingExecutionTime / this.startingExecutionTime) * 100)
				.setScale(2, BigDecimal.ROUND_HALF_UP)
				.floatValue();
	}
	
	public void decreaseRemainingExecutionTime() {
		this.remainingExecutionTime--;
	}

	@Override
	public String toString() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean equals(Object arg0) {
		Job otherJob = (Job) arg0;
		
		return this.jobId.equals(otherJob.getJobId());
	}
	
	
	
}
