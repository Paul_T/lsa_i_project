package cs.ucc.dc1.datacentre.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import cs.ucc.dc1.datacentre.Datacentre;
import cs.ucc.dc1.datacentre.Datacentre.CoolingSystemEnum;
import cs.ucc.dc1.datacentre.manager.migration.CPUStatus;
import cs.ucc.dc1.datacentre.thread.CPUThread;
import cs.ucc.dc1.utils.Configuration;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

/** Represents a CPU
 * <br>Stores information about a CPU and a thread simulating the execution of jobs in this CPU */
public class CPU {
	
	private int cpuLoad;
	private double cpuHistoricalUse;
	private double temperature;
	private String idCPU;
	private CPUStatus cpuStatus;
	private CPUThread cpuThread;
	private Datacentre datacentre;
	
	/** JAVA FX GUI **/
	private Label idLabel;
	private Label workloadLabel;
	private Label tempLabel;
	private TextArea consoleCPU;
	
	private ArrayList<Job> jobs = new ArrayList<Job>();

	public CPU(Datacentre datacentre) {
		this.datacentre = datacentre;
		
		this.idCPU = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 10);
		cpuLoad = 0;
		temperature = Configuration.CPU_MIN_TEMPERATURE;
		cpuHistoricalUse = 0;
		cpuStatus = CPUStatus.fine;
		
		cpuThread = new CPUThread(this);
		cpuThread.setIsRunning(true);
		cpuThread.start();
	}
	
	public int getCpuLoad() {
		return cpuLoad;
	}

	public void setCpuLoad(int cpuLoad) {
		this.cpuLoad = cpuLoad;
		updateLabels();
	}

	public double getCpuHistoricalUse() {
		return cpuHistoricalUse;
	}

	public void setCpuHistoricalUse(double cpuHistoricalUse) {
		this.cpuHistoricalUse = cpuHistoricalUse;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		
		if (temperature < 40) {
			temperature = 40d;
		}
		
		this.temperature = new BigDecimal(temperature)
				.setScale(1, BigDecimal.ROUND_HALF_UP)
				.doubleValue();
		updateLabels();
	}
	
	public Datacentre getDatacentre() {
		return datacentre;
	}
	
	/**
	 * Increment of decrement the temp of CPU depending on the DC cooling system
	 */
	public void updateTemperature(CoolingSystemEnum coolingSystem, int cpuLoad, double temperature, boolean isWorking) {
		
		if (coolingSystem.equals(CoolingSystemEnum.ON)) {
			if (temperature > Configuration.CPU_MIN_TEMPERATURE) {
				if (isWorking) {
					setTemperature(temperature - 0.1d);
				} else {
					// Refresh better when CPU is not working
					setTemperature(temperature - 2.0d);
				}
			}
		} else if (isWorking) {
			double increaseValue;
			
			// The more the CPU is loaded, the faster it gets hot
			if (cpuLoad < 50) {
				increaseValue = 1d;
			} else if (cpuLoad < 80) {				
				increaseValue = 2d;
			} else {
				increaseValue = 4d;
			}
			setTemperature(temperature + increaseValue);
		}
		
		datacentre.updateCoolingSystemIfNecessary();
	}

	public ArrayList<Job> getJobs() {
		return jobs;
	}
	
	public void executeJob(Job job) {
		
	}
	
	public String getIdCPU() {
		return idCPU;
	}

	public void setIdCPU(String idCPU) {
		this.idCPU = idCPU;
	}

	public CPUStatus getCpuStatus() {
		return cpuStatus;
	}

	public void setCpuStatus(CPUStatus cpuStatus) {
		this.cpuStatus = cpuStatus;
		updateLabels();
	}
	/** Called when a job is assigned to a CPU */
	public void assignJob(Job job) {
		addJob(job);
		startExecution(job);
	}
	/** Called when a job is migrated to a CPU */
	public void migrateJobToNewCPU(Job job) {
		addJob(job);
		startExecutionAfterMigration(job);
	}
	/** adds the job in the CPU and adapts the current CPU load*/
	private void addJob(Job job) {
		
		System.out.println("\t" + this.getClass().getName() + " : Job (" + job.getJobId() + ")");
		
		jobs.add(job);
		setCpuLoad(this.cpuLoad + job.getCpuLoad());
		this.cpuThread.release();
	}
	/** deletes the job from the CPU to send it to another one */
	public void migrateJob(Job job) {
		stopExecutionBeforeMigration(job);
		removeJob(job);
	}
	/** deletes the job when its execution is over*/
	public void endJob(Job job) {
		stopExecution(job);
		
		removeJob(job);
		this.datacentre.endJob(job);
	}
	/** removes the jobs and adapts the current CPU load*/
	private void removeJob(Job job) {
		jobs.remove(job);
		setCpuLoad(this.cpuLoad - job.getCpuLoad());
	}
	/** Sends a message to the cloud about the current execution of the job */
	public void sendProgressionToCloud(Job job) {
		this.datacentre.sendProgressionToCloud(job);
	}
	
	private void startExecution(Job job) {
		writeIntoConsole("Job (" + job.getJobId() + ") \t*** start execution ***");
	}
	private void startExecutionAfterMigration(Job job) {
		writeIntoConsole("Job (" + job.getJobId() + ") \t*** migrated and started ***");
	}
	private void stopExecution(Job job) {
		writeIntoConsole("Job (" + job.getJobId() + ") \t--- stop execution ---");
	}
	private void stopExecutionBeforeMigration(Job job) {
		writeIntoConsole("Job (" + job.getJobId() + ") \t--- stopped and migrated ---");
	}
	
	public void writeIntoConsole(String text) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				try {
					consoleCPU.appendText(text);
					consoleCPU.appendText("\n");
				}catch(Exception e) {
					
				}
			}
		});
	}

	/** Reschedules jobs of the CPU after a migration and a scheduling happened*/
	public void assignScheduledListOfJobs(List<Job> jobs) {
		System.out.println("\t" + this.getClass().getName());
		Boolean valid = false;
		for(int i = 0 ; i < this.jobs.size() ;i++) {
			if(!this.jobs.get(i).equals(jobs.get(i)))
				valid = true;
		}
		if(!valid)
			return; 

		writeIntoConsole("\t///   Scheduling performed \\\\\\");
		List<Job> deleteJobs = new ArrayList<Job>();
		deleteJobs.addAll(jobs);
		
		for(Job job : deleteJobs) {
			removeJob(job);
		}
		for(Job job : jobs) {
			addJob(job);
		}
	}
	
	@Override
	public String toString() {
		return this.idCPU + " - " + this.getCpuLoad() + " - " + this.cpuStatus;
	}

	public Label getIdLabel() {
		return idLabel;
	}

	public void setIdLabel(Label idLabel) {
		this.idLabel = idLabel;
	}

	public Label getWorkloadLabel() {
		return workloadLabel;
	}

	public void setWorkloadLabel(Label workloadLabel) {
		this.workloadLabel = workloadLabel;
	}

	public Label getTempLabel() {
		return tempLabel;
	}

	public void setTempLabel(Label tempLabel) {
		this.tempLabel = tempLabel;
	}

	public TextArea getConsoleCPU() {
		return consoleCPU;
	}

	public void setConsoleCPU(TextArea consoleCPU) {
		this.consoleCPU = consoleCPU;
	}
	
	public void updateLabels() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				try {
					idLabel.setText("CPU " + idCPU);
					tempLabel.setText("Temperature: " + temperature + " �C");
					workloadLabel.setText("Workload: " + cpuLoad + "%");
				} catch(Exception e) {
					
				}
			}
		});
	}
}
