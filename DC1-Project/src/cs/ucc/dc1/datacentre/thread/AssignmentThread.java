package cs.ucc.dc1.datacentre.thread;

import java.util.concurrent.Semaphore;

import cs.ucc.dc1.datacentre.Datacentre;

/** Assigns and migrates when new jobs arrive to the datacentre */
public class AssignmentThread extends Thread {
	
	private Semaphore semaphore;
	private boolean running;
	private Datacentre datacentre;
	
	public AssignmentThread(Datacentre datacentre) {
		this.datacentre = datacentre;
		this.semaphore = new Semaphore(0, true);
	}
	
	@Override
	public void run() {
		
		while (running) {
			try {
				semaphore.acquire();
				datacentre.assignAndMigrate();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void setRunning(boolean running) {
		this.running = running;
	}
	
	public void release() {
		semaphore.release();
	}

}
