package cs.ucc.dc1.datacentre;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import cs.ucc.dc1.cloud.Cloud;
import cs.ucc.dc1.datacentre.bean.CPU;
import cs.ucc.dc1.datacentre.bean.Job;
import cs.ucc.dc1.datacentre.dao.DatacentreDatabase;
import cs.ucc.dc1.datacentre.manager.JobAssigner;
import cs.ucc.dc1.datacentre.manager.Migrator;
import cs.ucc.dc1.datacentre.manager.Scheduler;
import cs.ucc.dc1.datacentre.manager.WorkloadMonitor;
import cs.ucc.dc1.datacentre.sensor.TemperatureMonitoringCPU;
import cs.ucc.dc1.datacentre.thread.AssignmentThread;
import cs.ucc.dc1.datacentre.thread.CloudListenerThread;
import cs.ucc.dc1.utils.Configuration;
import cs.ucc.dc1.utils.TestSettings;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

/** Represents a datacentre */
public class Datacentre {
	
	/** GUI JAVA FX **/
	private Stage dcStage;
	private Group root;
	private TextArea console;
	private Label coolingSystemLabel;
	
	/** GENERAL **/
	private String id;
	public enum CoolingSystemEnum {
		ON,
		OFF
	};
	
	private CoolingSystemEnum coolingSystem;
	private TemperatureMonitoringCPU temperatureMonitoringCPU = new TemperatureMonitoringCPU();
	
	/** COMMUNICATION **/
	private Cloud cloud;
	private CloudListenerThread incomingJobsThread;
	private AssignmentThread assignmentThread;
	private List<Job> waitingJobList = new ArrayList<>();
	private List<Job> newlyArrivedJobs = new ArrayList<Job>();
	
	private DatacentreDatabase datacentreDatabase = new DatacentreDatabase(TestSettings.NO_CPU_PER_DATACENTER,this);
	
	private WorkloadMonitor workloadMonitor;
	
	public synchronized void setCoolingSystem(CoolingSystemEnum coolingSystem) {
		this.coolingSystem = coolingSystem;
		updateLabels();
	}
	
	public synchronized CoolingSystemEnum getCoolingSystem() {
		return coolingSystem;
	}
	
	
	public void updateLabels() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				try {
					coolingSystemLabel.setText("Cooling System: " + coolingSystem.name());
				}catch(Exception e) {
					
				}
			}
		});
	}
	
	public Datacentre(Stage dcStage, Group root, TextArea console, String id,Cloud cloud) {
		this.dcStage = dcStage;
		this.root = root;
		this.console = console;
		this.id = id;
		this.workloadMonitor =  new WorkloadMonitor(this);
		this.cloud = cloud;
		
		incomingJobsThread = new CloudListenerThread(this);
		incomingJobsThread.setRunning(true);
		incomingJobsThread.start();
		
		this.assignmentThread = new AssignmentThread(this);
		assignmentThread.setRunning(true);
		assignmentThread.start();
		
		setCoolingSystem(CoolingSystemEnum.OFF);
	}
	
	public Datacentre() {
		this.workloadMonitor =  new WorkloadMonitor(this);
		setCoolingSystem(CoolingSystemEnum.OFF);
	}
	
	public void setupLabelsForCpu(int indexCPU, Label idLabel, Label workloadLabel, Label tempLabel, TextArea consoleCPU) {
		CPU cpu = datacentreDatabase.getCpus().get(indexCPU);
		
		cpu.setIdLabel(idLabel);
		cpu.setWorkloadLabel(workloadLabel);
		cpu.setTempLabel(tempLabel);
		cpu.setConsoleCPU(consoleCPU);
		cpu.updateLabels();
	}
	
	public void appendToJobList(Job job) {
		System.out.println(this.getClass().getName() + " : Job (" + job.getJobId() + ")");
		waitingJobList.add(job);
		
		if (Configuration.DC_TRIGGER_ASSIGNMENT_AND_MIGRATION <= waitingJobList.size()) {
			assignmentThread.release();
		}
	}
	
	public void updateCoolingSystemIfNecessary() {
		// Trigger or not the cooling system
		temperatureMonitoringCPU.monitorTemperature(this);
	}
	
	public List<Job> getWaitingJobList() {
		return waitingJobList;
	}

	/**
	 * Assigns the list of waiting jobs to valid CPUs and checks if a migration is required. If it is, the migration and the scheduling 
	 * are triggered.
	 */
	public void assignAndMigrate() {

		updateCoolingSystemIfNecessary();
		
		newlyArrivedJobs.addAll(waitingJobList);
		waitingJobList.clear();
		
		Scheduler scheduler = new Scheduler();
		Migrator migrator = new Migrator();
		JobAssigner jobAssigner = new JobAssigner(this);
		
		List<Job> jobToRemove = new ArrayList<Job>();
		Boolean isMigrationWorthIt = false;
		for(Job job : newlyArrivedJobs) {	
			if(workloadMonitor.isMigrationWorthIt(job))
				isMigrationWorthIt = true;
			
			workloadMonitor.calculateNewGlobalWorkload(job);
			
			if(workloadMonitor.getGlobalRunningWorkload() < Configuration.DC_MAX_ALLOWED_LOAD) {
				if(jobAssigner.assignToFirstValidCPU(job)) {
					jobToRemove.add(job);
				}
			}
		}
		
		for(Job job : jobToRemove) {
			newlyArrivedJobs.remove(job);
		}
		
		performMigration(isMigrationWorthIt, scheduler, migrator,false,false);
	}
	
	public void performMigration(boolean isMigrationWorthIt, Scheduler scheduler, Migrator migrator, Boolean test, Boolean value) {
		if(isMigrationWorthIt) {
			List<CPU> cpus = datacentreDatabase.getCpus();
			Boolean release = test ? value : migrator.migrate(cpus);
			for(CPU cpu : cpus) {
				if(cpu.getJobs().size() > 0) {
					cpu.assignScheduledListOfJobs(scheduler.scheduleJobs(cpu.getJobs()));
				}
			}
			
			if(release && !test) {
				if(this.assignmentThread != null)
					this.assignmentThread.release();	
			}
		}
	}

	public void endJob(Job job) {
		workloadMonitor.removeJobLoadFromDCLoad(job);
		this.assignmentThread.release();
	}
	
	public void sendProgressionToCloud(Job job) {
		
		String jobAsString = jobToString(job);
		if(jobAsString != null)
			cloud.simulateJobProgReception(jobAsString);
	}
	
	public DatacentreDatabase getDatacentreDatabase() {
		return datacentreDatabase;
	}

	public String getId() {
		return id;
	}

	public void simulateJobReception(String jobAsString) {
		incomingJobsThread.pushJob(jobAsString);
	}
	
	public void writeIntoConsole(String text) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				console.appendText(text);
				console.appendText("\n");
			}
		});
	}

	public Label getCoolingSystemLabel() {
		return coolingSystemLabel;
	}

	public void setCoolingSystemLabel(Label coolingSystemLabel) {
		this.coolingSystemLabel = coolingSystemLabel;
	}
	
	private String jobToString(Job job) {
		try {
			return new ObjectMapper().writeValueAsString(job);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

	public List<Job> getNewlyArrivedJobs() {
		return newlyArrivedJobs;
	}
	
	
	
}
